<?php
/**
 * Jugador
 */
class Usuario
{
  private $conexion=null;
  private $nombre;
  private $apellidos;
  private $edad;
  private $curso;

  function __construct()
  {
  }

  /*
  * Param entrada: array $_POST
  * Param salida: string con el $error
  *               - ""-> sin error
                  - "MSG"-> error encontrado
  */
  public function comprobarCampos($post){
    $error=null;
    if(!isset($post)||!isset($post["nombre"])||!isset($post["apellidos"])||!isset($post["edad"])||!isset($post["curso"])){
      $error="";
    }elseif($post["nombre"]==""){
      $error="No has introducido el nombre";
    }elseif($post["apellidos"]==""){
      $error="No has introducido el apellido";
    }elseif($post["edad"]==""){
      $error="No has introducido la edad";
    }elseif($post["curso"]==""){
      $error="No has introducido el curso";
    }else{
      $error=false;
      $this->nombre=$post["nombre"];
      $this->apellidos=$post["apellidos"];
      $this->edad=$post["edad"];
      $this->curso=$post["curso"];
    }
    return $error;
  }

  public function conectar(){
    $this->conexion = new mysqli("localhost", "root", "", "juegos");
    if ($this->conexion->connect_errno) {
        echo "Fallo al conectar a MySQL: (" . $this->conexion->connect_errno . ") " . $this->conexion->connect_error;
    }
  }
  public function insertarJugador(){
                $consulta="INSERT INTO usuarios (nombre, apellidos, edad, curso)
                VALUES ('$this->nombre', '$this->apellidos', $this->edad, $this->curso)";
                  $this->conexion->query($consulta);
  }
  public function listarJugadores(){
    $resultado=$this->conexion->query("SELECT id, nombre, apellidos, edad, curso, puntuacion FROM usuarios");
    return $resultado;
  }
}

 ?>
